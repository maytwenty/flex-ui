import React, { useState } from "react";
import "./App.css";
import ApplicationProcess from "./views/ApplicationProcess/ApplicationProcess";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link as RouterLink,
} from "react-router-dom";
import Admin from "./views/Admin/Admin";
import "./App.css";
import { FirebaseProvider } from "./Firebase";
import AlertProvider from "./components/Alert/AlertProvider";
import {
  AppBar,
  Drawer,
  IconButton,
  List,
  ListItem,
  ListItemText,
  makeStyles,
  Toolbar,
  Typography,
  Link,
} from "@material-ui/core";
import MenuIcon from "@material-ui/icons/Menu";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  appBar: {
    zIndex: theme.zIndex.modal + 1,
  },
  drawer: {
    width: 250,
    flexShrink: 0,
  },
  drawerPaper: {
    width: 250,
    marginTop: theme.spacing(8),
  },
}));

function App() {
  const classes = useStyles();
  const [open, setOpen] = useState<boolean>(false);

  const toggleDrawer = () => {
    setOpen(!open);
  };

  return (
    <div className={classes.root}>
      <FirebaseProvider>
        <AlertProvider>
          <Router>
            <AppBar position="static" className={classes.appBar}>
              <Toolbar>
                <IconButton
                  edge="start"
                  className={classes.menuButton}
                  color="inherit"
                  aria-label="menu"
                  onClick={toggleDrawer}
                >
                  <MenuIcon />
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                  Flex Legal
                </Typography>
              </Toolbar>
            </AppBar>
            <Drawer
              className={classes.drawer}
              classes={{
                paper: classes.drawerPaper,
              }}
              anchor="left"
              open={open}
              onClose={toggleDrawer}
            >
              <List>
                <Link component={RouterLink} to="/">
                  <ListItem button>
                    <ListItemText>Apply Now</ListItemText>
                  </ListItem>
                </Link>
                <Link component={RouterLink} to="/admin">
                  <ListItem button>
                    <ListItemText>Admin</ListItemText>
                  </ListItem>
                </Link>
              </List>
            </Drawer>
            <Switch>
              <Route path="/admin">
                <Admin />
              </Route>
              <Route path="/">
                <ApplicationProcess />
              </Route>
            </Switch>
          </Router>
        </AlertProvider>
      </FirebaseProvider>
    </div>
  );
}

export default App;
