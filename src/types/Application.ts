import { ApplicationStatus } from "./ApplicationStatus";

export interface Application {
    id: string;
    firstName: string;
    surname: string;
    email: string;
    telephone: string;
    status: ApplicationStatus;
    cv: string;
}