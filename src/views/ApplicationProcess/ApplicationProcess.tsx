import React from "react";
import { Container, Typography, Box } from "@material-ui/core";
import ApplicationForm from "../../components/ApplicationForm/ApplicationForm";

const ApplicationProcess: React.FC = () => {
  return (
    <Container maxWidth="md">
      <Box py={4}>
        <Typography variant="h5">Apply to join Flex Legal</Typography>
        <Box py={4}>
          <ApplicationForm />
        </Box>
      </Box>
    </Container>
  );
};

export default ApplicationProcess;
