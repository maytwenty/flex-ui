import React, { useState, useEffect } from "react";
import {
  Box,
  Container,
  FormControlLabel,
  Switch,
  Typography,
} from "@material-ui/core";
import { Application } from "../../types/Application";
import { useFirebase } from "../../Firebase";
import AdminTable from "../../components/AdminTable/AdminTable";
import { ApplicationStatus } from "../../types/ApplicationStatus";
import useAlert from "../../components/Alert/useAlert";

const Admin: React.FC = () => {
  const [applications, setApplications] = useState<Array<Application>>([]);
  const [pending, setPending] = useState<boolean>(true);
  const [loading, setLoading] = useState<boolean>(false);
  const firebase = useFirebase();
  const { showAlert } = useAlert();

  const getApplications = (pending: boolean) => {
    setLoading(true);
    setApplications([]);
    firebase
      .firestore()
      .collection("applications")
      .where(
        "status",
        "in",
        pending
          ? [ApplicationStatus.Pending]
          : [ApplicationStatus.Approved, ApplicationStatus.Rejected]
      )
      .get()
      .then((querySnapshot) => {
        let applications = querySnapshot.docs.map((doc) => {
          return doc.data() as Application;
        });
        setApplications(applications);
        setLoading(false);
      });
  };

  useEffect(() => {
    getApplications(pending);
  }, [pending]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setPending(event.target.checked);
  };

  const handleApprove = (
    status: ApplicationStatus,
    application: Application
  ) => {
    firebase
      .firestore()
      .collection("applications")
      .doc(application.id)
      .update({ status: status })
      .then(() => {
        showAlert(
          status,
          "You have successfully " + status + " the application."
        );
        getApplications(pending);
      });
  };

  const handleDownload = (application: Application) => {
    firebase
      .storage()
      .ref()
      .child(application.cv)
      .getDownloadURL()
      .then((url) => {
        window.open(url, "_blank");
      });
  };

  return (
    <Container>
      <Box py={4}>
        <Box display="flex">
          <Box flexGrow={1}>
            <Typography variant="h5">Applications Admin</Typography>
          </Box>
          <FormControlLabel
            label="Show Pending"
            labelPlacement="start"
            control={<Switch checked={pending} onChange={handleChange} />}
          />
        </Box>
        <Box py={4}>
          <AdminTable
            applications={applications}
            onApprove={handleApprove}
            onDownload={handleDownload}
            showActions={pending}
            loading={loading}
          ></AdminTable>
        </Box>
      </Box>
    </Container>
  );
};

export default Admin;
