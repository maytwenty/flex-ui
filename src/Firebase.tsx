import React, { useContext } from "react";
import firebase from "firebase/app";
import "firebase/firestore";
import "firebase/storage";

const firebaseConfig = {
  apiKey: "AIzaSyB9bywSVN6OrXqS8Od8IvIrQHNRg-wCcUk",
  authDomain: "flex-test-signup-form.firebaseapp.com",
  projectId: "flex-test-signup-form",
  storageBucket: "flex-test-signup-form.appspot.com",
  messagingSenderId: "425704250730",
  appId: "1:425704250730:web:79f78e42e41c7a4bd65913",
};

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
} else {
  firebase.app(); // if already initialized, use that one
}
const FirebaseContext = React.createContext(firebase);
export const FirebaseProvider: React.FC<{}> = ({ children }) => {
  return (
    <FirebaseContext.Provider value={firebase}>
      {children}
    </FirebaseContext.Provider>
  );
};
export const useFirebase = () => {
  return useContext(FirebaseContext);
};
