import { Button, Grid } from "@material-ui/core";
import React from "react";
import { ApplicationStatus } from "../../types/ApplicationStatus";
import CheckIcon from "@material-ui/icons/Check";
import CloseIcon from "@material-ui/icons/Close";

export interface ActionButtonsProps {
  onApprove: (status: ApplicationStatus) => void;
}

const ActionButtons: React.FC<ActionButtonsProps> = ({ onApprove }) => {
  return (
    <Grid container spacing={1}>
      <Grid item>
        <Button
          variant="contained"
          color="primary"
          onClick={() => onApprove(ApplicationStatus.Approved)}
          startIcon={<CheckIcon />}
        >
          Approve
        </Button>
      </Grid>
      <Grid item>
        <Button
          variant="contained"
          color="secondary"
          onClick={() => onApprove(ApplicationStatus.Rejected)}
          startIcon={<CloseIcon />}
        >
          Reject
        </Button>
      </Grid>
    </Grid>
  );
};

export default ActionButtons;
