import React, { useCallback, useState } from "react";

import AlertContext from "./AlertContext";
import Alert from "./Alert";

const AlertProvider: React.FC = ({ children }) => {
  const [dialogOpen, setDialogOpen] = useState<boolean>(false);
  const [title, setTitle] = useState<string>("");
  const [message, setMessage] = useState<string>("");

  const handleClose = () => {
    setDialogOpen(false);
  };

  const showAlert = useCallback((title, message) => {
    setTitle(title);
    setMessage(message);
    setDialogOpen(true);
  }, []);

  return (
    <AlertContext.Provider value={{ showAlert }}>
      {children}
      <Alert
        open={dialogOpen}
        onClose={handleClose}
        title={title}
        message={message}
      />
    </AlertContext.Provider>
  );
};

export default AlertProvider;
