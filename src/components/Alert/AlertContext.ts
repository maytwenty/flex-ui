import { createContext } from "react";

export interface AlertContextType {
	showAlert: (title: string, message: string) => void;
}

const AlertContext = createContext<(AlertContextType)>({ showAlert: () => {}});

export default AlertContext;