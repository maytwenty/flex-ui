import { Chip } from "@material-ui/core";
import React from "react";
import { ApplicationStatus } from "../../types/ApplicationStatus";

const getColor = (status: ApplicationStatus) => {
  switch (status) {
    case ApplicationStatus.Approved:
      return "primary";
    case ApplicationStatus.Rejected:
      return "secondary";
    case ApplicationStatus.Pending:
      return "default";
  }
};

export interface StatusBadgeProps {
  status: ApplicationStatus;
}

const StatusBadge: React.FC<StatusBadgeProps> = ({ status }) => {
  return <Chip label={status} variant="outlined" color={getColor(status)} />;
};

export default StatusBadge;
