import * as React from "react";
import { ColDef, DataGrid } from "@material-ui/data-grid";
import { Application } from "../../types/Application";
import { IconButton } from "@material-ui/core";
import { ApplicationStatus } from "../../types/ApplicationStatus";
import CloudDownloadIcon from "@material-ui/icons/CloudDownload";
import StatusBadge from "../StatusBadge/StatusBadge";
import ActionButtons from "../ActionButtons/ActionButtons";

export interface AdminTableProps {
  applications?: Array<Application>;
  onApprove: (status: ApplicationStatus, application: Application) => void;
  onDownload: (application: Application) => void;
  showActions: boolean;
  loading?: boolean;
}

const columns = ({
  onApprove,
  onDownload,
  showActions,
}: AdminTableProps): Array<ColDef> => {
  let columnDefinitions: Array<ColDef> = [
    { field: "firstName", headerName: "First name", flex: 0.5 },
    { field: "surname", headerName: "Surname", flex: 0.5 },
    { field: "email", headerName: "Email", flex: 0.5 },
    { field: "telephone", headerName: "Telephone", flex: 0.5 },
    {
      field: "status",
      headerName: "Status",
      width: 120,
      renderCell: (params) => {
        return <StatusBadge status={params.value as ApplicationStatus} />;
      },
    },
    {
      field: "cv",
      headerName: "CV",
      width: 100,
      renderCell: (params) => {
        return (
          <IconButton
            color="primary"
            onClick={() => onDownload(params.row as Application)}
          >
            <CloudDownloadIcon />
          </IconButton>
        );
      },
    },
  ];

  if (showActions) {
    return [
      ...columnDefinitions,
      {
        field: "id",
        headerName: "Action",
        flex: 1,
        renderCell: (params) => {
          return (
            <ActionButtons
              onApprove={(status) => {
                onApprove(status, params.row as Application);
              }}
            ></ActionButtons>
          );
        },
      },
    ];
  }

  return columnDefinitions;
};

const AdminTable: React.FC<AdminTableProps> = ({
  applications = [],
  onApprove,
  onDownload,
  showActions,
  loading = false,
}) => {
  return (
    <div style={{ height: 400, width: "100%" }}>
      <DataGrid
        rows={applications}
        columns={columns({ onApprove, onDownload, showActions })}
        pageSize={5}
        loading={loading}
      ></DataGrid>
    </div>
  );
};

export default AdminTable;
