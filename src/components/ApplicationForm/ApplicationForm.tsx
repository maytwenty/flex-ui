import { Button, FormControl, Grid } from "@material-ui/core";
import { Formik, FormikHelpers, Form, Field } from "formik";
import React from "react";
import * as yup from "yup";
import type { TypeOf } from "yup";
import { TextField, SimpleFileUpload } from "formik-material-ui";
import { useFirebase } from "../../Firebase";
import useAlert from "../Alert/useAlert";
import { Application } from "../../types/Application";
import { ApplicationStatus } from "../../types/ApplicationStatus";

const validationSchema = yup.object({
  firstName: yup.string().required("First name is required"),
  surname: yup.string().required("Surname is required"),
  email: yup
    .string()
    .email("Enter a valid email")
    .required("Email is required"),
  telephone: yup.string().required("Telephone number is required"),
  cv: yup.mixed().required("A file is required"),
});

interface ApplicationFormValues extends TypeOf<typeof validationSchema> {}

const ApplicationForm: React.FC = () => {
  const firebase = useFirebase();
  const { showAlert } = useAlert();

  const initialValues: ApplicationFormValues = {
    firstName: "",
    surname: "",
    email: "",
    telephone: "",
    cv: "",
  };

  const onSubmit = async (
    result: ApplicationFormValues,
    formikHelpers: FormikHelpers<ApplicationFormValues>
  ) => {
    firebase
      .storage()
      .ref()
      .child(result.cv.name)
      .put(result.cv)
      .then(() => {
        var ref = firebase.firestore().collection("applications").doc();

        let application: Application = {
          id: ref.id,
          status: ApplicationStatus.Pending,
          firstName: result.firstName || "",
          surname: result.surname || "",
          email: result.email || "",
          telephone: result.telephone || "",
          cv: result.cv.name,
        };

        ref.set(application).then(() => {
          formikHelpers.resetForm();
          showAlert(
            "Application Success!",
            "Thank you for submitting an application to Flex Legal"
          );
        });
      });
  };

  return (
    <>
      <Formik
        initialValues={initialValues}
        validationSchema={validationSchema}
        onSubmit={onSubmit}
      >
        {(formik) => (
          <Form>
            <Grid container spacing={3}>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <Field
                    name="firstName"
                    label="First Name"
                    component={TextField}
                    variant="outlined"
                    required
                  ></Field>
                </FormControl>
              </Grid>
              <Grid item xs={12} md={6}>
                <FormControl fullWidth>
                  <Field
                    name="surname"
                    label="Surname"
                    component={TextField}
                    variant="outlined"
                    required
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <Field
                    name="email"
                    label="Email"
                    component={TextField}
                    variant="outlined"
                    required
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <Field
                    name="telephone"
                    label="Telephone Number"
                    component={TextField}
                    variant="outlined"
                    required
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <FormControl fullWidth>
                  <Field
                    name="cv"
                    label="Upload CV"
                    component={SimpleFileUpload}
                    required
                  />
                </FormControl>
              </Grid>
              <Grid item xs={12}>
                <Button
                  variant="contained"
                  color="primary"
                  onClick={formik.submitForm}
                  disabled={!formik.isValid || !formik.dirty}
                >
                  Apply To Flex-Legal
                </Button>
              </Grid>
            </Grid>
          </Form>
        )}
      </Formik>
    </>
  );
};

export default ApplicationForm;
